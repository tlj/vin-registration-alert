FROM golang:alpine as builder

RUN mkdir /build

ADD . /build/

WORKDIR /build

RUN go build -o vin-registration-alert .

# PRODUCTION
FROM alpine

RUN adduser -S -D -H -h /app appuser

USER appuser

COPY --from=builder /build/vin-registration-alert /app/

WORKDIR /app

ENTRYPOINT ["./vin-registration-alert"]
CMD [""]