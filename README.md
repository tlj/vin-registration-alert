# Monitor VIN for registration at Vegvesenet

## Running

```shell script
docker run --rm -it -e ALERT_VIN=YOUR_VIN_HERE registry.gitlab.com/tlj/vin-registration-alert
```

## Environment variables

| Name                   | Description                                             |
|------------------------|---------------------------------------------------------|
| ALERT_VIN              | The VIN to monitor.                                     |
| SLACK_WEBHOOK          | Slack Webhook to post to when VIN is found (only once). |  
| CAR_IMAGE_URL          | The image to post to Slack when alerting.               |
| CHECK_INTERVAL_MINUTES | How often to check for registration (default 60).       |
