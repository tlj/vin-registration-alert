package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/ashwanthkumar/slack-go-webhook"
	_ "github.com/joho/godotenv/autoload"
	log "github.com/sirupsen/logrus"
)

type Kjoretoy struct {
	TeknNoxUtslippMgprkh   string `json:"tekn_nox_utslipp_mgprkh"`
	TeknFelg1              string `json:"tekn_felg_1"`
	TeknNgn                string `json:"tekn_ngn"`
	TeknSlagvolum          string `json:"tekn_slagvolum"`
	TeknNoxUtslippGprkwh   string `json:"tekn_nox_utslipp_gprkwh"`
	TeknLengde             string `json:"tekn_lengde"`
	TeknFelg2              string `json:"tekn_felg_2"`
	TeknFelg3              string `json:"tekn_felg_3"`
	TeknStandstoy          string `json:"tekn_standstoy"`
	TeknDekkF              string `json:"tekn_dekk_f"`
	TeknSistePkk           string `json:"tekn_siste_pkk"`
	TeknSitteplasserforan  string `json:"tekn_sitteplasserforan"`
	TeknMerkenavn          string `json:"tekn_merkenavn"`
	TeknThvMBrems          string `json:"tekn_thv_m_brems"`
	TeknBredde             string `json:"tekn_bredde"`
	TeknMinavstMs2         string `json:"tekn_minavst_ms2"`
	TeknHybrid             string `json:"tekn_hybrid"`
	TeknMinavstMs1         string `json:"tekn_minavst_ms1"`
	TeknHast3              string `json:"tekn_hast_3"`
	TeknHast2              string `json:"tekn_hast_2"`
	TeknEuHoved            string `json:"tekn_eu_hoved"`
	TeknCo2Utslipp         string `json:"tekn_co2_utslipp"`
	TeknDrivstoffForbruk   string `json:"tekn_drivstoff_forbruk"`
	TeknAntallDorer        string `json:"tekn_antall_dorer"`
	TeknPartikkelutslipp   string `json:"tekn_partikkelutslipp"`
	TeknEuronormNy         string `json:"tekn_euronorm_ny"`
	TeknHast1              string `json:"tekn_hast_1"`
	TeknSpor2              string `json:"tekn_spor_2"`
	TeknSpor3              string `json:"tekn_spor_3"`
	TeknEuVersjon          string `json:"tekn_eu_versjon"`
	TeknTotvekt            string `json:"tekn_totvekt"`
	TeknSpor1              string `json:"tekn_spor_1"`
	TeknTknavn             string `json:"tekn_tknavn"`
	TeknLuft3              string `json:"tekn_luft_3"`
	TeknLuft1              string `json:"tekn_luft_1"`
	TeknLuft2              string `json:"tekn_luft_2"`
	TeknAvregDato          string `json:"tekn_avreg_dato"`
	TeknSitteplasserTotalt string `json:"tekn_sitteplasser_totalt"`
	TeknPartikkelfilter    string `json:"tekn_partikkelfilter"`
	TeknGirkasse           string `json:"tekn_girkasse"`
	TeknHybridKategori     string `json:"tekn_hybrid_kategori"`
	TeknEuVariant          string `json:"tekn_eu_variant"`
	TeknDekk1              string `json:"tekn_dekk_1"`
	TeknKjtgrp             string `json:"tekn_kjtgrp"`
	TeknDekk2              string `json:"tekn_dekk_2"`
	TeknMerke              string `json:"tekn_merke"`
	TeknVogntogvekt        string `json:"tekn_vogntogvekt"`
	TeknMotorytelse        string `json:"tekn_motorytelse"`
	TeknMaksTaklast        string `json:"tekn_maks_taklast"`
	TeknThvUBrems          string `json:"tekn_thv_u_brems"`
	TeknModell             string `json:"tekn_modell"`
	TeknTukVerdi           string `json:"tekn_tuk_verdi"`
	TeknUnr                string `json:"tekn_unr"`
	TeknNestePkk           string `json:"tekn_neste_pkk"`
	TeknDrivst             string `json:"tekn_drivst"`
	TeknRegStatus          string `json:"tekn_reg_status"`
	TeknRegAar             string `json:"tekn_reg_aar"`
	TeknFGN                string `json:"tekn_f_g_n"`
	TeknLast2              string `json:"tekn_last_2"`
	TeknLast3              string `json:"tekn_last_3"`
	TeknDekk3              string `json:"tekn_dekk_3"`
	TeknLast1              string `json:"tekn_last_1"`
	TeknEuType             string `json:"tekn_eu_type"`
	TeknInnp1              string `json:"tekn_innp_1"`
	TeknInnp2              string `json:"tekn_innp_2"`
	TeknInnp3              string `json:"tekn_innp_3"`
	TeknBruktimp           string `json:"tekn_bruktimp"`
	TeknFarge              string `json:"tekn_farge"`
	TeknBelHFeste          string `json:"tekn_bel_h_feste"`
	TeknMili1              string `json:"tekn_mili_1"`
	TeknMili2              string `json:"tekn_mili_2"`
	TeknEgenvekt           string `json:"tekn_egenvekt"`
	TeknMili3              string `json:"tekn_mili_3"`
	TeknAkslerDrift        string `json:"tekn_aksler_drift"`
	TeknAksler             string `json:"tekn_aksler"`
	TeknMaalemetode        string `json:"tekn_maalemetode"`
}

type KjoretoyResponse struct {
	Entries []Kjoretoy `json:"entries"`
	Page  int `json:"page"`
	Pages int `json:"pages"`
	Posts int `json:"posts"`
}

func mustEnv(name string) string {
	val := os.Getenv(name)

	if val == "" {
		log.Fatalf("Environment variable '%s' must be set.", name)
	}

	return val
}

var (
	baseURL = "https://hotell.difi.no/api/json/vegvesen/kjoretoy?query="
	foundKjoretoy *Kjoretoy
)

func checkStatus(vin string) (*Kjoretoy, error) {
	url := baseURL + vin

	res, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("status code is %d. Expected %d", res.StatusCode, http.StatusOK)
	}

	var jres KjoretoyResponse
	dec := json.NewDecoder(res.Body)
	err = dec.Decode(&jres)
	if err != nil {
		return nil, err
	}

	if jres.Posts == 0 {
		return nil, nil
	}

	return &jres.Entries[0], nil
}

func main() {
	vin := mustEnv("ALERT_VIN")
	webhook := os.Getenv("SLACK_WEBHOOK")
	imageUrl := os.Getenv("CAR_IMAGE_URL")
	checkIntervalStr := os.Getenv("CHECK_INTERVAL_MINUTES")
	checkIntervalEnv, _ := strconv.Atoi(checkIntervalStr)

	checkIntervalMinutes := time.Duration(60)
	if checkIntervalEnv > 0 {
		checkIntervalMinutes = time.Duration(checkIntervalEnv)
	}

	log.Infof("Monitoring VIN %s...", vin)
	if webhook != "" {
		log.Infof("Will post alerts to %s.", webhook)
		if imageUrl != "" {
			log.Infof("Will use image %s.", imageUrl)
		}
	}
	log.Infof("Using interval %d minutes.", checkIntervalMinutes)

	checkInterval := checkIntervalMinutes * time.Minute

	t := time.NewTicker(time.Second)

	for {
		select {
		case <-t.C:
			if foundKjoretoy != nil {
				log.Infof("Found %s: %s - %s.", foundKjoretoy.TeknUnr, foundKjoretoy.TeknModell, foundKjoretoy.TeknRegStatus)
				continue
			}

			t.Stop()
			k, err := checkStatus(vin)
			if err != nil {
				log.Error(err)
			}
			t = time.NewTicker(checkInterval)

			if k == nil {
				log.Infof("%s not registered.", vin)
				continue
			}

			log.Infof("Found %s: %s - %s.", k.TeknUnr, k.TeknModell, k.TeknRegStatus)
			if webhook != "" {
				payload := slack.Payload{
					Text:      fmt.Sprintf("Found %s: %s - %s.", k.TeknUnr, k.TeknModell, k.TeknRegStatus),
					Channel:   "#alerts",
					IconEmoji: ":smile_cat:",
				}
				if imageUrl != "" {
					att := slack.Attachment{}
					att.ImageUrl = &imageUrl
					payload.Attachments = []slack.Attachment{att}
				}

				log.Infof("Posting to %s...", webhook)
				errs := slack.Send(webhook, "", payload)
				if len(errs) > 0 {
					log.Errorf("Error posting to slack: %v", errs)
					continue
				}
			}

			foundKjoretoy = k
		}
	}

}
